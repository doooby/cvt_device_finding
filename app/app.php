<?php

use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Response;
use \Doctrine\DBAL\Query\QueryBuilder;
use JasonGrimes\Paginator;

require_once 'config.php';
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'   => 'pdo_sqlsrv',
        'host' => DB_HOST,
        'dbname' => DB_DATABASE,
        'user' => DB_USER,
        'password' => DB_PASSWORD
    ),
));


$app->get('/', function (Request $request) use ($app) {
    if (!isset($_GET['q'])) {
        $query = '';
        $rows = [];

    } else {
        $query = $_GET['q'];

        $page = (isset($_GET['p']) ? intval($_GET['p']) : 1);
        $per_page = 10;
        $offset = ($page - 1) * $per_page;

        $sql = "
        SELECT COUNT(*) AS count
        FROM devices
        WHERE mac LIKE ?;
        ";
        $statement = $app['db']->prepare($sql);
        $statement->bindValue(1, "%{$query}%");
        $statement->execute();
        $total_count = $statement->fetchAll()[0]['count'];

        $sql = "
        SELECT *
        FROM devices
        WHERE mac LIKE ?
        ORDER BY mac
        OFFSET {$offset} ROWS
        FETCH NEXT {$per_page} ROWS ONLY;
        ";
        $statement = $app['db']->prepare($sql);
        $statement->bindValue(1, "%{$query}%");
        $statement->execute();
        $rows = $statement->fetchAll();

        $paginator = new Paginator($total_count, $per_page, $page, "/?q={$query}&p=(:num)");
        $paginator->setMaxPagesToShow(5);

    }

    require '../templates/browsing.php';
    return '';
});
