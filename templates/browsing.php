<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Vyhledávání zařízení</title>
    <link rel="stylesheet" type="text/css" href="/vendor/foundation.min.css">
    <script src="/vendor/jquery-3.1.1.slim.min.js"></script>
    <script src="/vendor/foundation.min.js"></script>
</head>
<body>

<div class="row">
    <div class="small-12 columns">
        <form action="/" method="get">
            <label> Vyhledat:
                <input type="text" name="q" placeholder="..." title="ip, mac, hostname" value="<?php echo $query ?>">
            </label>
        </form>
    </div>
</div>

<?php if(!empty($rows)) { ?>
  <table>
      <thead>
      <tr>
        <?php foreach ($rows[0] as $column => $_) { ?>
          <th><?php echo $column ?></th>
        <?php } ?>
      </tr>
      </thead>
      <tbody>
      <?php foreach ($rows as $row) { ?>
      <tr>
        <?php foreach ($row as $value) { ?>
          <td><?php echo $value ?></td>
        <?php } ?>
      </tr>
      <?php } ?>
      </tbody>
  </table>
<?php } ?>

<?php if (isset($paginator)) { ?>
  <div style="text-align: center;">
  <ul class="pagination">
      <?php if ($paginator->getPrevUrl()): ?>
          <li><a href="<?php echo $paginator->getPrevUrl(); ?>">&laquo; Previous</a></li>
      <?php endif; ?>

      <?php foreach ($paginator->getPages() as $page): ?>
          <?php if ($page['url']): ?>
              <li <?php echo $page['isCurrent'] ? 'class="current"' : ''; ?>>
                  <a href="<?php echo $page['url']; ?>"><?php echo $page['num']; ?></a>
              </li>
          <?php else: ?>
              <li class="disabled"><span><?php echo $page['num']; ?></span></li>
          <?php endif; ?>
      <?php endforeach; ?>

      <?php if ($paginator->getNextUrl()): ?>
          <li><a href="<?php echo $paginator->getNextUrl(); ?>">Next &raquo;</a></li>
      <?php endif; ?>
  </ul>
  </div>
<?php } ?>

<script>
    $(document).foundation();
</script>
</body>
</html>
